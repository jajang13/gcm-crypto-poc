run:
	ng serve

build-dev:
	docker build -t registry.digitalocean.com/prior-container/crypto-web:1.0.0 .
	docker push registry.digitalocean.com/prior-container/crypto-web:1.0.0

deploy-dev: build-dev
	kubectl replace --force -f .k8s/crypto_web.yml